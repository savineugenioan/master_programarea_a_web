﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Proiect
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }
        List<string> _nameList = new List<string>();
        List<string> _listSuggestion = null;

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            string cui = Box1.Text.Trim();
            if (verfif_CUI(cui))
            {
                Frame.Navigate(typeof(vizualizare), cui);
            }
            else
            {
                MessageDialog dialog = new MessageDialog("CUI-ul nu este intr-un format corect sau nu a fost gasit", "Eroare");
                dialog.Commands.Add(new UICommand("Ok", null));
                dialog.DefaultCommandIndex = 0;
                dialog.CancelCommandIndex = 1;
                await dialog.ShowAsync();
            }
        }

        public bool verfif_CUI(string cui)
        {
            if (!Int64.TryParse(cui, out long rez)) return false;
            if (cui.Length > 10) return false;
            long CUI = Int64.Parse(cui), v = 753217532; ;

            long c1 = CUI % 10;
            CUI /= 10;

            // executa operatiile pe cifre
            long t = 0;
            while (CUI > 0)
            {
                t += (CUI % 10) * (v % 10);
                CUI = (int)(CUI / 10);
                v = (int)(v / 10);
            }

            long c2 = t * 10 % 11;

            if (c2 == 10)
            {
                c2 = 0;
            }
            return c1 == c2;
        }

        private void Box1_SuggestionChosen(AutoSuggestBox sender, AutoSuggestBoxSuggestionChosenEventArgs args)
        {
            var selectedItem = args.SelectedItem.ToString();
            sender.Text = selectedItem;
        }

        private void Box1_TextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        {
            _listSuggestion = _nameList.Where(x => x.StartsWith(sender.Text)).ToList();
            sender.ItemsSource = _listSuggestion;
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            _nameList.Add("14837428");
            _nameList.Add("23070439");
            _nameList.Add("31845110");
            _nameList.Add("17113233");
            Box1.ItemsSource = _nameList;
        }


        private void Box1_QuerySubmitted(AutoSuggestBox sender, AutoSuggestBoxQuerySubmittedEventArgs args)
        {
            _listSuggestion = _nameList.Where(x => x.StartsWith(sender.Text)).ToList();
            sender.ItemsSource = _listSuggestion;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Map));
        }
    }
}
