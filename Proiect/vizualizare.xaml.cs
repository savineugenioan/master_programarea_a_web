﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Data.Json;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Proiect
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class vizualizare : Page
    {
        public vizualizare()
        {
            this.InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            x = (string)e.Parameter;
            Get_CUI();
        }
        public static string x="";
        public static string getFromTags(string response, string tag_name)
        {
            string x;
            x = response.Substring(0, response.IndexOf("</" + tag_name + ">"));
            x = x.Substring(response.IndexOf("<" + tag_name + ">") + ("<" + tag_name + ">").Count());
            return x;
        }
        public void Get_CUI()
        {
            string response = httpGet("http://api.bit-soft.ro/info.php?country=RO&company=" +
            x + "&key=4862014cf18bf9593824d368ee628ead");
            string phone = getFromTags(response, "phone");
            string name = getFromTags(response, "name");
            string country = getFromTags(response, "country");
            string city = getFromTags(response, "city");
            string address = getFromTags(response, "address");

            Phone.Text = phone;
            Name.Text = name;
            Country.Text = country;
            City.Text = city;
            Address.Text = address;
        }

        public static string httpGet(string uri)

        {

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);

            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())

            using (Stream stream = response.GetResponseStream())

            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }

        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(MainPage));
        }
    }
}
